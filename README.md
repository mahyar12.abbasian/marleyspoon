# README
## Description

This project is written using ruby on rails framework as its backend and vuejs framework to power the frontend side of it.

The code structure is as follows:

### app/models/recipe.rb
The model recipe is created and uses [Contentful Model](https://github.com/contentful/contentful_model) gem to connect recipe model directly to 'recipe' content in Contentful.

### app/controllers/panel/v1
These api controller is used for vuejs to call actions and show recipes and their detail. The index action calls [Contentful Model](https://github.com/contentful/contentful_model) function using pagination and returns the json array of all recipes returned from contentful. The show action requests specific recipe by id and returns the full object of recipe as json.

### app/javascript
This folder contains all the vuejs logic and uses redux structure there is only on page designed in /pages/admins/index.vue

### app/lib/scopes.rb
A module that is written to use [Contentful Model](https://github.com/contentful/contentful_model) easier and well tested. These are like scopes that can be used in rails models. Unfortunately in [Contentful Model](https://github.com/contentful/contentful_model) the scopes are not accessible.

### specs
For test rspec is used and all tests are in this folder

## Run the project
```
git clone https://gitlab.com/mahyar12.abbasian/marleyspoon
cd marleyspoon
```
```javascript
yarn
```
```ruby
bundle install
gem install foreman
foreman start
```

Then locate your browser to localhost:3000 and you must see the loading procedure and then the list of recipes. By clicking on each recipe the full object will be requested from server and will be shown when object is receved. Before locating to localhost:3000 make sure the vue webpack-dev-server is running from terminal and the loader compiled the files (The first time takes some seconds to load).