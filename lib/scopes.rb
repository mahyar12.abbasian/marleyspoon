module Scopes
  def find_by_id(object, id)
    object.find(id) if id.present?
  end

  def paginate_order_by(object, page = 1, per_page = 100, order_by = "")
    object.paginate(page = page, per_page = per_page, order_field = order_by, additional_options = {}).load if page.present? and per_page.present?
  end

  def serialized_object(object, fields)
    response = {}
    fields.each do |field|
      if not field.kind_of? (String)
        field.keys.each do |key|
          if eval("object.#{key.to_s}").present?
            response[key.to_s] = serialized_array eval("object.#{key.to_s}"), field[key]
          end
        end
      else
        if eval("object.#{field.split(".")[0]}").present?
          response[field.gsub(".", "_")] = eval("object.#{field}")
        end
      end
    end
    response
  end

  def serialized_array(array, fields)
    return [] if array.nil?
    array.map do |data|
      serialized_object(data, fields)
    end
  end
end
