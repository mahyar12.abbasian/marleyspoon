module MarleySpoonExceptions
  class DataExceptionsParams < StandardError
    def initialize(msg = "This is a custom exception", code = 100, exception_type = "invalid params")
      super(msg)
      @exception_type = exception_type
      @code = code
    end

    def code
      return @code
    end

    def exception_type
      return @exception_type
    end
  end
end
