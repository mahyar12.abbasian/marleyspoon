ContentfulRails.configure do |config|
  config.authenticate_webhooks = true # false here would allow the webhooks to process without basic auth
  config.access_token = "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c"
  config.space = "kk2bw5ojx476"
  config.environment = "master"
  config.eager_load_entry_mapping = false
  config.contentful_options = {
    entry_mapping: {
      "recipe" => Recipe,
    },
  }
end
