require "api_constraints"
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :panel do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :recipes, only: [:index, :show]
      get "/*path", to: "base#admins", as: :admins
    end
  end
  root to: "panel/v1/base#admins"
end
