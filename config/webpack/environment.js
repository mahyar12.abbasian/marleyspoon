const { environment } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

const vue = require('./loaders/vue')
const sass = require('./loaders/sass')
const scss = require('./loaders/scss')
// const file = require('./loaders/file')
// const json = require('./loaders/json')
// environment.loaders.delete('moduleSass');
// environment.loaders.delete('moduleCss');
// environment.loaders.delete('sass');

// environment.loaders.get('css').use.find((el) => el.loader === 'style-loader').loader = 'vue-style-loader';


environment.plugins.prepend('VuetifyLoaderPlugin', new VuetifyLoaderPlugin());
environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())
environment.loaders.prepend('vue', vue)
environment.loaders.prepend('sass', sass)
environment.loaders.prepend('scss', scss)
// environment.loaders.prepend('file', file)
// environment.loaders.prepend('json', json)

module.exports = environment
