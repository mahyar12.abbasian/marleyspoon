module.exports = {
  test: /\.json$/,
  type: 'javascript/auto',
  use: [{
    loader: 'json-loader'
  }]
}
