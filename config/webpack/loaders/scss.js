// webpack.config.js

module.exports = {    
  test: /\.scss$/,
  use: [
    'vue-style-loader',
    'css-loader',
    {
      loader: 'sass-loader',
      // Requires sass-loader@^8.0.0
      options: {
        // This is the path to your variables
        implementation: require('sass'),
        sassOptions: {
          fiber: require('fibers'),
          indentedSyntax: true // optional
        },
        prependData: "@import 'app/javascript/assets/css/sass/variables.scss'"
      },
    },
  ],
}