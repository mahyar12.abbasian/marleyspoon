# == Schema Information
#
# Table name: answer_questions
#
#  id          :bigint(8)        not null, primary key
#  correct     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  answer_id   :integer
#  question_id :integer
#

require "rails_helper"
require "scopes"

RSpec.describe Recipe, type: :model do
  include Scopes

  describe "Find recipe" do
    it "should return recipe by given id" do
      recipe = find_by_id(Recipe, "437eO3ORCME46i02SeCW46")
      expect(recipe).not_to be_nil
      expect(recipe.id).to eq("437eO3ORCME46i02SeCW46")
    end
    it "should raise exception if id does not exist" do
      expect(find_by_id(Recipe, "test")).to be_nil
    end
  end

  describe "Paginate recipes" do
    it "should return recipes by default" do
      recipes = paginate_order_by(Recipe)
      expect(recipes).to be
      expect(recipes.count).to be <= 100
      expect(recipes.skip).to eq(0)
    end
    it "should return recipes by page" do
      recipes = paginate_order_by(Recipe, 2, 2)
      expect(recipes).to be
      expect(recipes.count).to be <= 2
      expect(recipes.skip).to eq(2)
      expect(recipes.limit).to eq(2)
    end
  end

  describe "Serialize recipe object" do
    it "should return json object" do
      recipe = find_by_id(Recipe, "437eO3ORCME46i02SeCW46")
      serialized = serialized_object(recipe, ["id", "title"])
      expect(serialized).not_to be_nil
      expect(serialized["id"]).not_to be_nil
      expect(serialized["title"]).not_to be_nil
    end
    it "should return nested objects as json key" do
      recipe = find_by_id(Recipe, "437eO3ORCME46i02SeCW46")
      serialized = serialized_object(recipe, ["chef.name"])
      expect(serialized).not_to be_nil
      expect(serialized["chef_name"]).not_to be_nil
    end
    it "should return nested array as json array" do
      recipe = find_by_id(Recipe, "437eO3ORCME46i02SeCW46")
      serialized = serialized_object(recipe, [tags: ["name"]])
      expect(serialized).not_to be_nil
      expect(serialized["tags"]).not_to be_nil
      expect(serialized["tags"].count).to be > 0
      expect(serialized["tags"][0]["name"]).not_to be_nil
    end
  end

  describe "Serialize recipes array" do
    it "should return json array" do
      recipes = paginate_order_by(Recipe)
      serialized = serialized_array(recipes, ["id"])
      expect(serialized).not_to be_nil
      expect(serialized).to be_a_kind_of(Array)
      expect(serialized.count).to be >= 0
      expect(serialized[0]["id"]).not_to be_nil
    end
  end
end
