require "rails_helper"

RSpec.describe Recipe, :type => :request do
  describe "GET index" do
    it "has a 200 status code" do
      headers = { "ACCEPT" => "application/json" }
      get "/panel/v1/recipes", :headers => headers

      expect(response.content_type).to eq("application/json")
      expect(response).to have_http_status(200)
    end
  end
  describe "GET show" do
    it "has a 200 status code" do
      headers = { "ACCEPT" => "application/json" }
      get "/panel/v1/recipes/437eO3ORCME46i02SeCW46", :headers => headers

      expect(response.content_type).to eq("application/json")
      expect(response).to have_http_status(200)
    end
  end
end
