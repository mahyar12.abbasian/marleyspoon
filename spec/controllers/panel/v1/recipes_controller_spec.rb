require "rails_helper"
require "marleyspoon/exceptions"
describe Panel::V1::RecipesController do
  include MarleySpoonExceptions
  describe "Index" do
    it "returns recipes" do
      get :index, params: {
                page: 1,
                per_page: 100,
              }
      json = JSON.parse(response.body)
      expect(json["result"]).to eq("OK")
      expect(json["recipes"]).to be_a_kind_of(Array)
    end
  end

  describe "Show" do
    it "returns recipe" do
      get :show, params: {
                   id: "437eO3ORCME46i02SeCW46",
                 }

      json = JSON.parse(response.body)
      expect(json["result"]).to eq("OK")
      expect(json["recipe"]["id"]).to eq("437eO3ORCME46i02SeCW46")
    end
    it "returns error" do
      get :show, params: {
                   id: "random",
                 }

      json = JSON.parse(response.body)
      expect(json["error"]["code"]).to eq(501)
    end
  end
end
