require "rails_helper"

RSpec.describe "Main page route", type: :routing do
  it "routes to main page" do
    expect(get: "/").to route_to("panel/v1/base#admins")
  end
end
