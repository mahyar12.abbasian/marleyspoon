require "rails_helper"

RSpec.describe Recipe, type: :routing do
  it "routes /panel/recipes to the recipes controller" do
    expect(get: "/panel/recipes").to route_to("panel/v1/recipes#index")
  end

  it "routes /panel/recipes/:id to the recipes controller" do
    expect(get("panel/recipes/123")).to route_to(
      controller: "panel/v1/recipes",
      action: "show",
      id: "123",
    )
  end

  it "does not route to delete recipes" do
    expect(delete("/panel/recipes/123")).not_to be_routable
  end

  it "does not route to update recipes" do
    expect(put("/panel/recipes/123")).not_to be_routable
  end

  it "does not route to create recipes" do
    expect(post("/panel/recipes")).not_to be_routable
  end
end
