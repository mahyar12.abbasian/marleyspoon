module Panel
  module V1
    class RecipesController < Panel::V1::BaseController
      # skip_before_action :verify_authenticity_token
      before_action :find_recipe, only: %i(show)

      def index
        @recipes = paginate_order_by(Recipe, recipe_params[:page], recipe_params[:per_page], recipe_params[:order_by])
        raise DataExceptionsParams.new(t("errors.problem_getting_all_recipes"), 502, "recipe") if @recipes.nil?
        render json: { result: "OK", recipes: serialized_array(@recipes, ["id", "title", "photo.url"]), meta: { total: @recipes.total, limit: @recipes.limit, skip: @recipes.skip } }
      end

      def show
        render json: { result: "OK", recipe: serialized_object(@recipe, ["id", "title", "photo.url", "description", "chef.name", tags: ["name"]]) }
      end

      private

      def find_recipe
        @recipe = find_by_id Recipe, params[:id]
        raise DataExceptionsParams.new(t("errors.recipe_not_exists"), 501, "recipe") if @recipe.nil?
      end

      def recipe_params
        params.permit(:page, :per_page, :order_by).delete_if { |key, value| value.blank? }
      end
    end
  end
end
