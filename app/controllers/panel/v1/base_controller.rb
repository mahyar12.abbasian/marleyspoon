require "scopes"

class Panel::V1::BaseController < ActionController::Base
  require "marleyspoon/exceptions"
  include Scopes
  include MarleySpoonExceptions

  rescue_from ::StandardError, with: :error_occurred
  rescue_from DataExceptionsParams, with: :error

  def admins
    @user_asstes_name = "admins"
    render template: "layouts/dashboard"
  end

  def error(e)
    logger.error e
    render json: { result: "ERROR", error: { message: e.message, code: e.code, exception_type: e.exception_type }, status: 404 }
  end

  def error_occurred(e)
    puts e
    logger.error e.message
    e.backtrace.each { |line|
      puts line
      logger.error line
    }
    render json: { result: "ERROR", error: { message: "Unknown Error.", code: 3 }, status: 404 }
  end
end
