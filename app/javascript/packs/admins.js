import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import router from '../routers/admins'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuetify from 'vuetify'

import Layout from '../pages/layout/template.vue'
import store from '../store/index'
import apiRoutes from '../api/api_routes'
import * as api from '../api'

import VueMeta from 'vue-meta'
import globalMethods from './global_methods'
import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";

import * as VueGoogleMaps from "vue2-google-maps"

import '../assets/css/auth.scss'
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/dist/vuetify.min.css'
import '../assets/css/common.scss'
import '../assets/css/main.css'
import "chartist/dist/chartist.min.css";
import "../assets/css/sass/overrides.sass";
import "../assets/css/vuetify.css"

Vue.mixin(globalMethods)

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(require("vue-chartist"));
// Vue.use(ElementUI, { locale })
// Vue.use(VueI18n);
Vue.use(VueAxios, axios)


const requireComponent = require.context("../components/base", true, /\.vue$/);

requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName);

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\//, "").replace(/\.\w+$/, ""))
  );

  Vue.component(
    `Base${componentName}`,
    componentConfig.default || componentConfig
  );
});

Vue.use(Vuetify)

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAj_ewuooTR993bp3OSF0YgBYsivB0b6NQ",
    // key: "",
    libraries: "places" // necessary for places input
  }
});

const theme = {

  primary: "#565bea",
  primaryColorLight: "#8f88ff",
  primaryColorDark: "#0031b7",
  textOnPrimaryColor: "#fff",
  textOnPrimaryColorDark: "#fff",
  secondary: "#8b70f1",
  secondaryColorLight: "#bf9fff",
  secondaryColorDark: "#5744be",
  textOnSecondaryColor: "#000000",

  // primary: '#4CAF50',
  // secondary: '#9C27b0',
  // accent: '#9C27b0',
  // info: '#00CAE3',
}

Vue.prototype.$apiRoutes = apiRoutes
document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    router,
    store,
    vuetify: new Vuetify({
      icons: {
        iconfont: 'mdi',
      },      
      theme: {
        themes: {
          dark: theme,
          light: theme,
        },
      },
	}),
    render: createEl => createEl(Layout),
  }).$mount('#app')
})
