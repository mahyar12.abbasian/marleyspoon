import routes from '../routes/admins'
import VueRouter from 'vue-router'

const router = new VueRouter({
  mode: 'history',
  base: '/panel/admins',
  routes,
})


export default router
