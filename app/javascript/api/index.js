import axios from 'axios'
import apiRoutes from './api_routes'
import recipe from './recipe'


const securedAxiosInstance = axios.create({
  baseURL: apiRoutes.base,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json'
  }
})

const plainAxiosInstance = axios.create({
  baseURL: apiRoutes.base,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json'
  }
})

securedAxiosInstance.interceptors.request.use(config => {
  const method = config.method.toUpperCase()
  if (method !== 'OPTIONS' && method !== 'GET') {
    config.headers = {
      ...config.headers,
      'X-CSRF-TOKEN': localStorage.csrf
    }
  }
  return config
})

securedAxiosInstance.interceptors.response.use(null, error => {
  if (error.response && error.response.config && error.response.status === 401) {
    // If 401 by expired access cookie, we do a refresh request
    return auth.refreshToken();
  } else {
    return Promise.reject(error)
  }
})

export { securedAxiosInstance, plainAxiosInstance, recipe }
  