import apiRoutes from './api_routes'
// import { securedAxiosrInstance, plainAxiosInstance } from './index'
import axios from 'axios'

const recipe = {
	findRecipe (recipe_id, cb, errcb) {
    axios.get(apiRoutes.recipe + '/' + recipe_id)
      .then(response => {
        console.log(response)
        cb(response.data)
      })
      .catch((e) => {
        console.log(e)
        errcb({result: "ERROR"})
      })
  },

  getAllRecipes (params, cb, errcb) {
    axios.get(apiRoutes.recipe + params)
      .then(response => {
        console.log(response)
        cb({result: "OK", data: response.data})
      })
      .catch((e) => {
        console.log(e)
        errcb({result: "ERROR"})
      })
  }
}

export default recipe;