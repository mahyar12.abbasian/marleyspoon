// import { deserialize } from 'jsonapi-deserializer'
import {recipe} from '../../api'

const recipeActions = {  
  getAllRecipes ({commit}, {params, cb}) {
    recipe.getAllRecipes(params, (resp) => {
      console.log("getAllRecipes>>>>>",resp.data)
      commit('setRecipes', {data: resp.data.recipes, meta: resp.data.meta})
      cb(resp)
    })
  },
  findRecipe ({commit}, {recipe_id, cb}) {
    recipe.findRecipe(recipe_id, (resp) => {
      commit('setRecipe', resp.recipe);
      cb(resp);
    })
  }
}

export default recipeActions;