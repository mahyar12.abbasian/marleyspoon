const recipeMutation = {
  setRecipes (state, recipesData) {
    state.recipes = recipesData.data
    state.recipesSize = recipesData.meta.total
    state.paginateOptions = recipesData.meta
  },
  setRecipe (state, recipe) {
    state.recipes = state.recipes.map((r)=>{
      if(r.id == recipe.id)
        return {...r, ...recipe}
      return r
    })
  },
}

export default recipeMutation;