import recipeGetters from '../getters/recipe';
import recipeMutations from '../mutations/recipe';
import recipeActions from '../actions/recipe';


const state = {
  recipes: [],
  recipesSize: 0,
  paginateOptions: {},
}

export default {
  namespaced: true,
  state,
  getters: recipeGetters,
  mutations: recipeMutations,
  actions: recipeActions,
};
